
let express = require('express');
let app = express();	
let bodyParser = require('body-parser');  // processa corpo de requests
let cookieParser = require('cookie-parser');  // processa cookies
let server = require('http').Server(app);

let time = setInterval(showTime, 1000);


app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded( { extended: true } ));
app.use(cookieParser());
socketio = require('socket.io')(server);
require('./server/routes/irc-Routes')(app,socketio);

server.listen(3000, function () {				
  console.log('Example app listening on port 3000!');	
});

function showTime(){
  var date = new Date();
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var seconds = date.getSeconds();
  console.log( hours + ":" + minutes + ":" + seconds);
};