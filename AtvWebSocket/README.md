- Exercício 1) Toda mensagem enviada pelo usuário é repetida (ecoada) na tela do próprio chat;
    
    Feito

- Exercício 2) O sistema envia regularmente, de segundo em segundo, uma mensagem com o horário do servidor;
    
    Feito

- Exercício 3) O servidor recebe uma expressão aritmética e retorna o resultado. Dica: utilize a função eval();
    
    Feito
   `/calc 1+2+3*8-9`

- Exercício 4) Modifique o tutorial de chat WebSockets de forma a receber o nick de usuário;

    Feito
    `/nick novo nick name`

- Exercício 5) Modifique o tutorial de chat WebSockets de forma a permitir o envio de um link de uma figura;

