let irc = require('irc');
const proxies = {}; // mapa de proxys
let proxy_id = 0;

module.exports =  {

    proxy:(id, servidor, nick, canal, socket) => {
        let ws = socket;

        let irc_client = new irc.Client(
                servidor, 
                nick,
                {channels: [canal],});

        irc_client.addListener('message', (from, canal, message)=> {
            console.log(from + ' => '+ canal +': ' + message);
            ws.emit("message", {"timestamp":Date.now(), 
            "nick":from,
            "msg":message});
        });
        
        irc_client.addListener('error', (message) =>{
            console.log('error: ', message);
        });

    	irc_client.addListener('mode', (message)=>{
    	    console.log('mode: ', message);
    	});


        irc_client.addListener('nick', (oldnick, newnick, channels, message) => {
            ws.emit('nick', oldnick, newnick, channels, message);
        });

        proxies[id] = { "ws":socket, "irc_client":irc_client  };

        return proxies[id];
    },

    incrementIdProxy:()=>{
        proxy_id++;
        return proxy_id;
    },

    proxySay:(proxy_id,canal,message)=>{
        proxies[proxy_id].irc_client.say(canal, message);
    },

    getCache:(proxy_id)=>{
        return proxies[proxy_id].cache
    },

    getClient:(proxy_id)=>{
        return proxies[proxy_id].irc_client;
    },
}