let path = require('path');	// módulo usado para lidar com caminhos de arquivos
let proxy = require('../api/proxys');
let commands = require('../api/commands');
//let socketio = require('../api/socketio');

module.exports = (app,socketio) => {
    app.get('/',(req, res) => {

      // commands.sendTime();

        let { servidor, nick, canal } = req.cookies;    
        if ( servidor && nick  && canal ) {
        let proxy_id = proxy.incrementIdProxy();
        socketio.on("connection", socket => {
            
            var p =	proxy.proxy(proxy_id,
                    servidor,
                    nick, 
                    canal,
                    socket);
                    
                socket.on("message", function (data) {
                   if(data[0]=="/"){
                    let command = String(data).trim();
                    let args = command.split(" ");
                    //console.log(commands.getCommand(args[0]));
                   }else{
                    console.log("mensagem:"+data);
                    proxy.proxySay(proxy_id,req.cookies.canal, data)
                   // proxies[proxy_id].irc_client.say(req.cookies.canal, data);
                   }

                
                
            })
        });
    
        res.cookie('id', proxy_id);
          res.sendFile(path.join(__dirname, '../../client/view/index.html'));
      }
      else {
            res.sendFile(path.join(__dirname, '../../client/view/login/login.html'));
      }
    });
    app.get('/obter_mensagem/:timestamp', (req, res) => {
        var id = req.cookies.id;
        res.append('Content-type', 'application/json');
        res.send(proxy.getCache("proxy_id"));
    });
    app.post('/gravar_mensagem',  (req, res) => {
        var irc_client = proxy.getClient(req.cookies.id);
        irc_client.say(req.cookies.canal, irc_client.opt.channels[0], req.body.msg );
        res.end();
    });
    app.get('/mode/:usuario/:args', (req, res) => {
        var usuario = req.params.usuario;
        var args = req.params.args;
        var retorno = '{"usuario":'+usuario+','+
                  '"args":"'+args+'}';
        var irc_client = proxy.getClient(req.cookies.id);
        var retorno = irc_client.send("mode", usuario, args);
        res.send(retorno);
    });
    app.post('/login', (req, res) => { 
        let {nome,canal,servidor} = req.body;
        res = createCookie(nome,canal,servidor,res);
        res.redirect('/');
    });
    app.get('/mode/', (req, res) => {
        var irc_client = proxy.getClient(req.cookies.id);
        var retorno = irc_client.send("mode", req.cookies.nick);
        res.send(retorno);
    });

    app.post('/result_command', (req,res)=>{
        let respCommand;
        let {command, params} = req.body; 
        let {id, nick, canal, servidor, io} = req.cookies;
        if(command=="/nick"){
            respCommand = commands.getCommand(command,params,id);
            let msg = nick + " agora é: " + respCommand;
            let n_id=id, n_nick=nick, n_canal=canal, n_servidor=servidor;
            res = deleteCookie(res);
            //res = createCookie(n_nick,n_canal,n_servidor,res);
            res.send({respCommand:msg});
        }else{
            respCommand = commands.getCommand(command,params,id);
            res.send({respCommand});
        }
    });
};

function createCookie(nome,canal,servidor,res){
    res.cookie('nick', nome);
    res.cookie('canal', canal);
    res.cookie('servidor', servidor);
    return res;
}

function deleteCookie(res){
    res.clearCookie('id');
    res.clearCookie('nick');
    res.clearCookie('canal');
    res.clearCookie('servidor');
    res.clearCookie('io');
    return res;
}